# Spring Cloud Sample Client that depends on other applications#

The application obtains its configuration from the spring cloud configuration server (localhost:8001), registers with Eureka registry and uses the Eureka registry to find other applications to call.