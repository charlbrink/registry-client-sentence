package za.brinkc.registry.client.service;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Mockito.when;

import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.test.context.junit4.SpringRunner;

import rx.Observable;

@RunWith(SpringRunner.class)
public class SentenceServiceImplTest {

	private static final String I_RAN_THE_REASONABLE_BOAT = "I ran the reasonable boat";

	@Mock
	private WordService wordService;

	@InjectMocks
	private SentenceService sentenceService = new SentenceServiceImpl(wordService);

	@Test
	public void when_sentenceServiceGetSentence_then_expect_Success() {
		when(wordService.getArticle()).thenReturn(Observable.just("the"));
		when(wordService.getAdjective()).thenReturn(Observable.just("reasonable"));
		when(wordService.getNoun()).thenReturn(Observable.just("boat"));
		when(wordService.getVerb()).thenReturn(Observable.just("ran"));
		when(wordService.getSubject()).thenReturn(Observable.just("I"));
		assertThat("Expected " + I_RAN_THE_REASONABLE_BOAT, sentenceService.getSentence(),
				equalTo(I_RAN_THE_REASONABLE_BOAT));
	}

}
