package za.brinkc.registry.client.service;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static org.hamcrest.CoreMatchers.hasItem;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpStatus.OK;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;

import rx.Observable;
import rx.observers.TestSubscriber;
import za.brinkc.registry.client.AdjectiveClient;
import za.brinkc.registry.client.Application;
import za.brinkc.registry.client.ArticleClient;
import za.brinkc.registry.client.NounClient;
import za.brinkc.registry.client.SubjectClient;
import za.brinkc.registry.client.VerbClient;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class,
		WordServiceImplIntegrationTest.TestServicesConfiguration.class }, webEnvironment = WebEnvironment.RANDOM_PORT)
public class WordServiceImplIntegrationTest {

	private static final String SUBJECT = "You";
	private static final String VERB = "bounce";
	private static final String ARTICLE = "the";
	private static final String ADJECTIVE = "red";
	private static final String NOUN = "ball";

	@Autowired
	public ArticleClient articleClient;

	@Autowired
	private AdjectiveClient adjectiveClient;

	@Autowired
	private NounClient nounClient;

	@Autowired
	private SubjectClient subjectClient;

	@Autowired
	private VerbClient verbClient;

	@Autowired
	private WordService wordService = new WordServiceImpl(articleClient, adjectiveClient, nounClient, subjectClient,
			verbClient);

	@ClassRule
	public static WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.wireMockConfig().dynamicPort());

	@Test
	public void when_wordServiceGetArticle_then_expect_Success()
			throws InterruptedException, ExecutionException, TimeoutException {
		stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(OK.value()).withBody(ARTICLE)));

		TestSubscriber<String> subscriber = new TestSubscriber<>();
		Observable<String> observable = wordService.getArticle();
		observable.subscribe(subscriber);

		subscriber.assertCompleted();
		subscriber.assertNoErrors();
		subscriber.assertValueCount(1);
		assertThat("Expected " + ARTICLE, subscriber.getOnNextEvents(), hasItem(ARTICLE));
	}

	@Test
	public void when_wordServiceGetAdjective_then_expect_Success() throws InterruptedException, ExecutionException {
		stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(OK.value()).withBody(ADJECTIVE)));

		TestSubscriber<String> subscriber = new TestSubscriber<>();
		Observable<String> observable = wordService.getAdjective();
		observable.subscribe(subscriber);

		subscriber.assertCompleted();
		subscriber.assertNoErrors();
		subscriber.assertValueCount(1);
		assertThat("Expected " + ADJECTIVE, subscriber.getOnNextEvents(), hasItem(ADJECTIVE));
	}

	@Test
	public void when_wordServiceGetNoun_then_expect_Success() throws InterruptedException, ExecutionException {
		stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(OK.value()).withBody(NOUN)));

		TestSubscriber<String> subscriber = new TestSubscriber<>();
		Observable<String> observable = wordService.getNoun();
		observable.subscribe(subscriber);

		subscriber.assertCompleted();
		subscriber.assertNoErrors();
		subscriber.assertValueCount(1);
		assertThat("Expected " + NOUN, subscriber.getOnNextEvents(), hasItem(NOUN));
	}

	@Test
	public void when_wordServiceGetVerb_then_expect_Success() throws InterruptedException, ExecutionException {
		stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(OK.value()).withBody(VERB)));

		TestSubscriber<String> subscriber = new TestSubscriber<>();
		Observable<String> observable = wordService.getVerb();
		observable.subscribe(subscriber);

		subscriber.assertCompleted();
		subscriber.assertNoErrors();
		subscriber.assertValueCount(1);
		assertThat("Expected " + VERB, subscriber.getOnNextEvents(), hasItem(VERB));
	}

	@Test
	public void when_wordServiceGetSubject_then_expect_Success() throws InterruptedException, ExecutionException {
		stubFor(get(urlEqualTo("/")).willReturn(aResponse().withStatus(OK.value()).withBody(SUBJECT)));

		TestSubscriber<String> subscriber = new TestSubscriber<>();
		Observable<String> observable = wordService.getSubject();
		observable.subscribe(subscriber);

		subscriber.assertCompleted();
		subscriber.assertNoErrors();
		subscriber.assertValueCount(1);
		assertThat("Expected " + SUBJECT, subscriber.getOnNextEvents(), hasItem(SUBJECT));
	}

	@Configuration
	@EnableFeignClients(basePackages = "za.brinkc.registry.client")
	static class TestServicesConfiguration {

		@Bean
		public ServerList<Server> ribbonServerList() {
			return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
		}
	}

}
