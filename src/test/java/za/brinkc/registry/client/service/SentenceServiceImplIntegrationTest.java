package za.brinkc.registry.client.service;

import static com.github.tomakehurst.wiremock.client.WireMock.aResponse;
import static com.github.tomakehurst.wiremock.client.WireMock.exactly;
import static com.github.tomakehurst.wiremock.client.WireMock.get;
import static com.github.tomakehurst.wiremock.client.WireMock.getRequestedFor;
import static com.github.tomakehurst.wiremock.client.WireMock.stubFor;
import static com.github.tomakehurst.wiremock.client.WireMock.urlEqualTo;
import static com.github.tomakehurst.wiremock.client.WireMock.verify;
import static org.hamcrest.CoreMatchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.http.HttpStatus.OK;

import java.util.concurrent.ExecutionException;
import java.util.concurrent.TimeoutException;

import org.junit.ClassRule;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.context.embedded.LocalServerPort;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.boot.test.context.SpringBootTest.WebEnvironment;
import org.springframework.cloud.netflix.feign.EnableFeignClients;
import org.springframework.cloud.netflix.ribbon.StaticServerList;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.scheduling.annotation.EnableAsync;
import org.springframework.test.context.junit4.SpringRunner;

import com.github.tomakehurst.wiremock.core.WireMockConfiguration;
import com.github.tomakehurst.wiremock.junit.WireMockRule;
import com.github.tomakehurst.wiremock.stubbing.Scenario;
import com.netflix.loadbalancer.Server;
import com.netflix.loadbalancer.ServerList;

import za.brinkc.registry.client.AdjectiveClient;
import za.brinkc.registry.client.Application;
import za.brinkc.registry.client.ArticleClient;
import za.brinkc.registry.client.NounClient;
import za.brinkc.registry.client.SubjectClient;
import za.brinkc.registry.client.VerbClient;

@RunWith(SpringRunner.class)
@SpringBootTest(classes = { Application.class,
		SentenceServiceImplIntegrationTest.TestServicesConfiguration.class }, webEnvironment = WebEnvironment.RANDOM_PORT)
public class SentenceServiceImplIntegrationTest {

	private static final String SUBJECT = "We";
	private static final String VERB = "kick";
	private static final String ARTICLE = "a";
	private static final String ADJECTIVE = "round";
	private static final String NOUN = "ball";
	private static final String SENTENCE = String.format("%s %s %s %s %s", SUBJECT, VERB, ARTICLE, ADJECTIVE, NOUN);

	@LocalServerPort
	private int port;

	@Autowired
	public ArticleClient articleClient;

	@Autowired
	private AdjectiveClient adjectiveClient;

	@Autowired
	private NounClient nounClient;

	@Autowired
	private SubjectClient subjectClient;

	@Autowired
	private VerbClient verbClient;

	@Autowired
	private WordService wordService = new WordServiceImpl(articleClient, adjectiveClient, nounClient, subjectClient,
			verbClient);

	@Autowired
	private SentenceService sentenceService = new SentenceServiceImpl(wordService);

	@ClassRule
	public static WireMockRule wireMockRule = new WireMockRule(WireMockConfiguration.wireMockConfig().dynamicPort());

	@Test
	public void when_sentenceServiceGetSentence_then_expect_Success()
			throws InterruptedException, ExecutionException, TimeoutException {
		WireMockConfiguration.wireMockConfig().port(port);
		stubFor(get(urlEqualTo("/")).inScenario("Sentence").whenScenarioStateIs(Scenario.STARTED)
				.willReturn(aResponse().withStatus(OK.value()).withBody(SUBJECT)).willSetStateTo(SUBJECT));
		stubFor(get(urlEqualTo("/")).inScenario("Sentence").whenScenarioStateIs(SUBJECT)
				.willReturn(aResponse().withStatus(OK.value()).withBody(VERB)).willSetStateTo(VERB));
		stubFor(get(urlEqualTo("/")).inScenario("Sentence").whenScenarioStateIs(VERB)
				.willReturn(aResponse().withStatus(OK.value()).withBody(ARTICLE)).willSetStateTo(ARTICLE));
		stubFor(get(urlEqualTo("/")).inScenario("Sentence").whenScenarioStateIs(ARTICLE)
				.willReturn(aResponse().withStatus(OK.value()).withBody(ADJECTIVE)).willSetStateTo(ADJECTIVE));
		stubFor(get(urlEqualTo("/")).inScenario("Sentence").whenScenarioStateIs(ADJECTIVE)
				.willReturn(aResponse().withStatus(OK.value()).withBody(NOUN)).willSetStateTo(NOUN));

		String sentence = sentenceService.getSentence();
		verify(exactly(5), getRequestedFor(urlEqualTo("/")));
		assertThat("Expected " + SENTENCE, sentence, equalTo(SENTENCE));
	}

	@Configuration
	@EnableAsync
	@EnableFeignClients(basePackages = "za.brinkc.registry.client")
	static class TestServicesConfiguration {

		@Bean
		public ServerList<Server> ribbonServerList() {
			return new StaticServerList<>(new Server("localhost", wireMockRule.port()));
		}

	}

}
