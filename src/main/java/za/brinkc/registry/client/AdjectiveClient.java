package za.brinkc.registry.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("${registry-client-adjective-app-name:registry-client-adjective}")
public interface AdjectiveClient {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getAdjective();

}
