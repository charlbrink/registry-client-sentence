package za.brinkc.registry.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("${registry-client-subject-app-name:registry-client-subject}")
public interface SubjectClient {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getSubject();

}
