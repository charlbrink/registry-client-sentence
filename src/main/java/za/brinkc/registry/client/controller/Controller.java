package za.brinkc.registry.client.controller;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.ResponseBody;
import org.springframework.web.bind.annotation.RestController;

import za.brinkc.registry.client.service.SentenceService;

@RestController
public class Controller {

	private SentenceService sentenceService;

	@Autowired
	public Controller(SentenceService sentenceService) {
		this.sentenceService = sentenceService;
	}

	@RequestMapping("/sentence")
	public @ResponseBody String getSentence() {
		return sentenceService.getSentence();
	}

}
