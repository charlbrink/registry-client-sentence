package za.brinkc.registry.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("${registry-client-noun-app-name:registry-client-noun}")
public interface NounClient {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getNoun();

}
