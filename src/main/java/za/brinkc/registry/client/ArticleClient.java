package za.brinkc.registry.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("${registry-client-article-app-name:registry-client-article}")
public interface ArticleClient {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getArticle();

}
