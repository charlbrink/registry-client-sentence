package za.brinkc.registry.client.service;

import java.util.ArrayList;
import java.util.List;
import java.util.concurrent.CountDownLatch;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.scheduling.annotation.Async;
import org.springframework.stereotype.Service;

import rx.Observable;

@Service
public class SentenceServiceImpl implements SentenceService {
	private WordService wordService;

	@Autowired
	public SentenceServiceImpl(final WordService wordService) {
		this.wordService = wordService;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see za.brinkc.registry.client.service.SentenceServiceI#getSentence()
	 */
	@Override
	public String getSentence() {
		List<Observable<String>> observables = createObservables();
		CountDownLatch latch = new CountDownLatch(observables.size());

		List<String> words = new ArrayList<>();
		Observable.merge(observables).subscribe((word) -> {
			words.add(word);
			latch.countDown();
		});

		try {
			latch.await();
		} catch (InterruptedException ex) {
			ex.printStackTrace();
		}

		return String.format("%s %s %s %s %s", words.toArray());
	}

	private List<Observable<String>> createObservables() {
		List<Observable<String>> observables = new ArrayList<>();
		observables.add(getSubject());
		observables.add(getVerb());
		observables.add(getArticle());
		observables.add(getAdjective());
		observables.add(getNoun());

		return observables;
	}

	@Async
	private Observable<String> getSubject() {
		return wordService.getSubject();
	}

	@Async
	private Observable<String> getVerb() {
		return wordService.getVerb();
	}

	@Async
	private Observable<String> getArticle() {
		return wordService.getArticle();
	}

	@Async
	private Observable<String> getAdjective() {
		return wordService.getAdjective();
	}

	@Async
	private Observable<String> getNoun() {
		return wordService.getNoun();
	}

}
