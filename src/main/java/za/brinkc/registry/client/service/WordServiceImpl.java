package za.brinkc.registry.client.service;

import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import com.netflix.hystrix.contrib.javanica.annotation.HystrixCommand;
import com.netflix.hystrix.contrib.javanica.annotation.HystrixProperty;

import rx.Observable;
import rx.Subscriber;
import za.brinkc.registry.client.AdjectiveClient;
import za.brinkc.registry.client.ArticleClient;
import za.brinkc.registry.client.NounClient;
import za.brinkc.registry.client.SubjectClient;
import za.brinkc.registry.client.VerbClient;

@Service
public class WordServiceImpl implements WordService {
	private ArticleClient articleClient;
	private AdjectiveClient adjectiveClient;
	private NounClient nounClient;
	private SubjectClient subjectClient;
	private VerbClient verbClient;
	private static final String DEFAULT_SUBJECT = "I";
	private static final String DEFAULT_VERB = "ran";
	private static final String DEFAULT_ARTICLE = "the";
	private static final String DEFAULT_ADJECTIVE = "reasonable";
	private static final String DEFAULT_NOUN = "boat";

	@Autowired
	public WordServiceImpl(final ArticleClient articleClient, final AdjectiveClient adjectiveClient,
			final NounClient nounClient, final SubjectClient subjectClient, final VerbClient verbClient) {
		this.articleClient = articleClient;
		this.adjectiveClient = adjectiveClient;
		this.nounClient = nounClient;
		this.verbClient = verbClient;
		this.subjectClient = subjectClient;
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see za.brinkc.registry.client.service.WordServiceI#getArticle()
	 */
	@Override
	@HystrixCommand(fallbackMethod = "defaultArticle", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "10"),
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "1000") })
	public Observable<String> getArticle() {
		return Observable.create(new Observable.OnSubscribe<String>() {

			@Override
			public void call(Subscriber<? super String> observer) {
                if (!observer.isUnsubscribed()) {
                	observer.onNext(articleClient.getArticle());
                    observer.onCompleted();
                }
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see za.brinkc.registry.client.service.WordServiceI#getAdjective()
	 */
	@Override
	@HystrixCommand(fallbackMethod = "defaultAdjective", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "10"),
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "1000") })
	public Observable<String> getAdjective() {
		return Observable.create(new Observable.OnSubscribe<String>() {

			@Override
			public void call(Subscriber<? super String> observer) {
                if (!observer.isUnsubscribed()) {
                    observer.onNext(adjectiveClient.getAdjective());
                    observer.onCompleted();
                }
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see za.brinkc.registry.client.service.WordServiceI#getNoun()
	 */
	@Override
	@HystrixCommand(fallbackMethod = "defaultNoun", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "10"),
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "1000") })
	public Observable<String> getNoun() {
		return Observable.create(new Observable.OnSubscribe<String>() {

			@Override
			public void call(Subscriber<? super String> observer) {
                if (!observer.isUnsubscribed()) {
                    observer.onNext(nounClient.getNoun());
                    observer.onCompleted();
                }
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see za.brinkc.registry.client.service.WordServiceI#getVerb()
	 */
	@Override
	@HystrixCommand(fallbackMethod = "defaultVerb", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "10"),
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "1000") })
	public Observable<String> getVerb() {
		return Observable.create(new Observable.OnSubscribe<String>() {

			@Override
			public void call(Subscriber<? super String> observer) {
                if (!observer.isUnsubscribed()) {
                    observer.onNext(verbClient.getVerb());
                    observer.onCompleted();
                }
			}
		});
	}

	/*
	 * (non-Javadoc)
	 * 
	 * @see za.brinkc.registry.client.service.WordServiceI#getSubject()
	 */
	@Override
	@HystrixCommand(fallbackMethod = "defaultSubject", commandProperties = {
			@HystrixProperty(name = "circuitBreaker.errorThresholdPercentage", value = "10"),
			@HystrixProperty(name = "circuitBreaker.sleepWindowInMilliseconds", value = "1000") })
	public Observable<String> getSubject() {
		return Observable.create(new Observable.OnSubscribe<String>() {

			@Override
			public void call(Subscriber<? super String> observer) {
                if (!observer.isUnsubscribed()) {
                    observer.onNext(subjectClient.getSubject());
                    observer.onCompleted();
                }
			}
		});
	}

	public String defaultArticle() {
		return DEFAULT_ARTICLE;
	}

	public String defaultAdjective() {
		return DEFAULT_ADJECTIVE;
	}

	public String defaultNoun() {
		return DEFAULT_NOUN;
	}

	public String defaultSubject() {
		return DEFAULT_SUBJECT;
	}

	public String defaultVerb() {
		return DEFAULT_VERB;
	}

}
