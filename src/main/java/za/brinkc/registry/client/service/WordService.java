package za.brinkc.registry.client.service;

import rx.Observable;

public interface WordService {

	Observable<String> getArticle();

	Observable<String> getAdjective();

	Observable<String> getNoun();

	Observable<String> getVerb();

	Observable<String> getSubject();

}