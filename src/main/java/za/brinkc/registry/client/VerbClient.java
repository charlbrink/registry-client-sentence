package za.brinkc.registry.client;

import org.springframework.cloud.netflix.feign.FeignClient;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestMethod;

@FeignClient("${registry-client-verb-app-name:registry-client-verb}")
public interface VerbClient {

	@RequestMapping(value = "/", method = RequestMethod.GET)
	public String getVerb();

}
